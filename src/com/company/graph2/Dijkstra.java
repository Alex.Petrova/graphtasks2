package com.company.graph2;

import java.util.ArrayDeque;
import java.util.HashMap;

public class Dijkstra {

    public HashMap<Integer, Integer> goDijkstra(int[][] adjacencyMatrix, int startIndex) {

        ArrayDeque<Integer> dequeNode = new ArrayDeque<>();
        HashMap<Integer, Integer> mapNode = new HashMap<>();


        mapNode.put(startIndex,0);
        int start = startIndex;
        dequeNode.addLast(startIndex);

        while (!dequeNode.isEmpty()) {

            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[startIndex][i] != 0) {
                    if (!mapNode.containsKey(i)) {
                        dequeNode.addLast(i);
                        mapNode.put(i, adjacencyMatrix[startIndex][i]+mapNode.getOrDefault(startIndex,0));
                    } else {
                        if((mapNode.get(startIndex) != null)&(mapNode.get(i) != null)) {
                            if ((mapNode.get(startIndex) + adjacencyMatrix[startIndex][i]) < mapNode.get(i)) {
                                mapNode.put(i, mapNode.get(startIndex) + adjacencyMatrix[startIndex][i]);
                            }
                        }
                    }
                }
            }

            dequeNode.pollFirst();
            if (!dequeNode.isEmpty()) {
                startIndex = dequeNode.peekFirst();
            }
        }
        mapNode.remove(start);
        return mapNode;
    }
}
