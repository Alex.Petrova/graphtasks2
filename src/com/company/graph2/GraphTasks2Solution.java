package com.company.graph2;

import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        Dijkstra dijkstra = new Dijkstra();
        return dijkstra.goDijkstra(adjacencyMatrix,startIndex);
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        Prim prim = new Prim();
        return prim.searchPrim(adjacencyMatrix);
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        Crascal crascal = new Crascal();
        return crascal.ostovCrascalGo(adjacencyMatrix);
    }
}
