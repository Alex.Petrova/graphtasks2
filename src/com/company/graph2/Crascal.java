package com.company.graph2;

import java.util.ArrayList;
import java.util.Collections;

public class Crascal {

    public int ostovCrascalGo(int[][] matrix) {

        ArrayList<Edge> edges = new ArrayList<>();

        int eCount = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if ((i < j)&(matrix[i][j] != 0)) {
                    edges.add(new Edge(i, j, matrix[i][j]));
                    eCount++;
                }
            }
        }

        SetNumbers setNumbers = new SetNumbers(eCount);

        Collections.sort(edges);

        int result = 0;
        for (Edge edge : edges) {
            if (setNumbers.unification(edge.u, edge.v)) {
                result += edge.weight;
            }
        }
        return result;
    }


    public class SetNumbers {
        int[] setNum;

        SetNumbers(int size) {
            setNum = new int[size];
            for (int i = 0; i < size; i++) {
                setNum[i] = i;
            }
        }

        int getSet(int x) {
            if (x == setNum[x]) {
                return x;
            } else {
                setNum[x] = getSet(setNum[x]);
                return setNum[x];
            }
        }

        boolean unification(int u, int v) {
            u = getSet(u);
            v = getSet(v);
            if (u == v) return false;
            for (int i = 0; i < setNum.length; i++) {
                if (setNum[i] == v) {
                    setNum[i] = u;
                }
            }
            return true;
        }
    }
}
