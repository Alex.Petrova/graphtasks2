package com.company.graph2;

import java.util.HashSet;

public class Prim {

    public int searchPrim(int[][] matrix) {
        HashSet<Integer> oneSet = new HashSet<>();
        HashSet<Integer> twoSet = new HashSet<>();

        int weight = 0;

        oneSet.add(0);
        for (int i = 1; i < matrix.length; i++) {
            twoSet.add(i);
        }

        int k = 0;
        while (oneSet.size() != matrix.length) {

            int min = Integer.MAX_VALUE;

            for (int sTwo : twoSet) {
                for (int sOne : oneSet) {
                    if ((matrix[sTwo][sOne] <= min) & (matrix[sTwo][sOne] != 0)) {
                        min = matrix[sTwo][sOne];
                        k = sTwo;
                    }
                }
            }

            twoSet.remove(k);
            oneSet.add(k);
            weight += min;
        }
        return weight;
    }
}
