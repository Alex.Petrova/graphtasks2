package com.company.graph2;

public class Edge implements Comparable<Edge> {

    int u;
    int v;
    int weight;

    Edge(int u, int v, int w) {
        this.u = u;
        this.v = v;
        this.weight = w;
    }

    @Override
    public int compareTo(Edge edge) {
        if (weight != edge.weight) {
            return weight < edge.weight ? -1 : 1;
        }
        return 0;
    }
}
